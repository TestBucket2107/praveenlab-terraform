output "clb_dns_name" {
  value = module.web-service.clb_dns_name
}

output "asg_name" {
  value = module.web-service.asg_name
}