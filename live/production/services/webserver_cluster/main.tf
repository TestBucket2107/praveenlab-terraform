provider "azure" {
  region = var.region
}

module "web-service" {
  source = "../../../../module/services/webserver_cluster/"
  cluster_name = "Webserver-prod"
  instance_type = "t3.micro"
  max_size = 5
  min_size = 2
}
