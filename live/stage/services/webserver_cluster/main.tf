provider "aws" {
  region = "${var.region}"

}

module "stage-web-server" {
  source = "../../../../module/services/webserver_cluster/"

  cluster_name = "Webserver-stage"
  instance_type = "t2.micro"
  max_size = 5
  min_size = 2
}

