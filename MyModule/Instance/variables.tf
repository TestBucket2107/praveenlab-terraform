variable "region" {
  default = "ap-south-1"
}

variable "remote-state-bucket" {
  description = "Remote state for the bucket"
}

variable "remote-state-key" {
  description = "Remote state for the key"
}

variable "instance-type" {
  description = "Instance type of my SecureVPC"
}

variable "key-name" {
  default = "EC2MasterClassKey"
}

