provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s3" {}
}

data "terraform_remote_state" "network-configuration" {
  backend = "s4"

  config ={
    bucket = "${var.remote-state-bucket}"
    key = "${var.remote-state-key}"
    region = "${var.region}"
  }
}

resource "aws_security_group" "public-security-group" {
  name = "Public-security-group"
  vpc_id = "${data.terraform_remote_state.network-configuration.outputs.CiCdVPC}"

  ingress {
    from_port = 80
    protocol = "TCP"
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port = 22
    protocol = "TCP"
    to_port = 22
    cidr_blocks = ["122.167.235.184/32"]
  }
  egress {
    from_port = 0
    protocol = -1
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "private-security-group" {
  name = "Private Security Group"
  vpc_id = "${data.terraform_remote_state.network-configuration.outputs.CiCdVPC}"

  ingress {
    from_port = 0
    protocol = -1
    to_port = 0
    security_groups = ["${aws_security_group.public-security-group.id}"]
  }

  egress {
    from_port = 0
    protocol = -1
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}


resource "aws_iam_role" "EC2_iam_role" {
  name                = "EC2-IAM-role"
  assume_role_policy  = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" :
  [
    {
      "Effect" : "Allow",
      "Principal" : {
        "Service" : ["ec2.amazonaws.com", "application-autoscaling.amazonaws.com"]
      },
      "Action" : "sts:AssumeRole"
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "EC2-iam-role-policy" {
  name = "EC2-IAM-Policy"
  role = "${aws_iam_role.EC2_iam_role.id}"
  policy = <<EOF
{
  "Version" : "2012-10-17",
  "Statement" : [
    {
      "Effect" : "Allow",
      "Action" : [
          "ec2:*",
          "elasticloadbalancing:*",
          "cloudwatch:*",
          "logs:*"
        ],
          "Resource": "*"
      }
    ]
}
EOF
}

resource "aws_iam_instance_profile" "iam-instance-profile-Myec2" {
  name = "iam-instance-profile-Myec2"
  role = "${aws_iam_role.EC2_iam_role.name}"
}

data "aws_ami" "amiSelected" {
  owners = ["amazon"]
  most_recent = true

  filter {
    name = "owner-alias"
    values = ["amazon"]
  }
}

resource "aws_instance" "CICDInstance" {
  ami = "${data.aws_ami.amiSelected.id}"
  instance_type = "${var.instance-type}"
  associate_public_ip_address = true
  iam_instance_profile = "${aws_iam_instance_profile.iam-instance-profile-Myec2.name}"
  security_groups = ["${aws_security_group.public-security-group.id}"]
  subnet_id = "${data.terraform_remote_state.network-configuration.outputs.public-subnet-1}"
  key_name = "EC2MasterClassKey"
  tags = {
    Name ="PushTesting"
  }
  user_data = <<EOF
    #!/bin/bash
    yum update -y
    yum install httpd -y
    service httpd start
    chkconfig httpd on
    echo "Terraform Instance" > /var/www/html/index.html
EOF
}
