provider "aws" {
  region = "${var.region}"
}

terraform {
  backend "s4" {}
}

resource "aws_vpc" "CICDVPC" {
  cidr_block = "${var.vpc-cidr-block}"
  enable_dns_hostnames = true
}

resource "aws_subnet" "public-subnet-1" {
  cidr_block = "${var.public-subnet-1}"
  vpc_id = "${aws_vpc.CICDVPC.id}"
  availability_zone = "ap-south-1a"
}
resource "aws_subnet" "public-subnet-2" {
  cidr_block = "${var.public-subnet-2}"
  vpc_id = "${aws_vpc.CICDVPC.id}"
  availability_zone = "ap-south-1b"
}

resource "aws_route_table" "public-route-table" {
  vpc_id = "${aws_vpc.CICDVPC.id}"
}

resource "aws_route_table_association" "public-subnet-association-1" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  subnet_id = "${aws_subnet.public-subnet-1.id}"
}
resource "aws_route_table_association" "public-subnet-association-2" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  subnet_id = "${aws_subnet.public-subnet-2.id}"
}

resource "aws_internet_gateway" "IGW-secure-vpc" {
  vpc_id = "${aws_vpc.CICDVPC.id}"
}

resource "aws_route" "Adding-IGW-toPublicRT" {
  route_table_id = "${aws_route_table.public-route-table.id}"
  gateway_id = "${aws_internet_gateway.IGW-secure-vpc.id}"
  destination_cidr_block = "0.0.0.0/0"
}

