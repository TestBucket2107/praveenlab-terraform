variable "region" {
  default = "ap-south-1"
  description = "My AWS region"
}
variable "vpc-cidr-block" {
  description = "cidr block for vpc"
}

variable "public-subnet-1" {
  description = "public-subnet-1"
}

variable "public-subnet-2" {
  description = "public-subnet-2"
}
